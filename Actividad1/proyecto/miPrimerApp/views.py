from django.shortcuts import render, HttpResponse
from .models import *
from django.db.models import Count
# Create your views here.

def index(request):
    grupo1 = Estudiante.objects.filter(grupo = 1)
    grupo4 = Estudiante.objects.filter(grupo = 4)

    apellidos = Estudiante.objects.values('apellidos').annotate(count=Count('id')).filter(count__gt=1)
    mismoApellido = {}
    for ap in apellidos:
        mismoApellido[ap['apellidos']] = Estudiante.objects.filter(apellidos = ap['apellidos'])

    edad = Estudiante.objects.values('edad').annotate(count=Count('id')).filter(count__gt=1)
    mismaEdad = {}
    for e in edad:
        mismaEdad[e['edad']] = Estudiante.objects.filter(edad = e['edad'])
    
    edades3 = Estudiante.objects.filter(grupo=3).values('edad').annotate(count=Count('id')).filter(count__gt=1)
    grupo3mismaEdad = {}
    for e in edades3:
        grupo3mismaEdad[e['edad']] = Estudiante.objects.filter(edad = e['edad'], grupo= 3)

    todos = Estudiante.objects.all()
    return render(request, "index.html", {
            "grupo1": grupo1,
            "grupo4": grupo4,
            "mismoApellido": mismoApellido,
            "mismaEdad": mismaEdad,
            "todos": todos,
            "mismaEdadg3": grupo3mismaEdad
        })